package com.example.emily.map;

/**
 * Created by Emily on 30/04/2017.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class Connect {

    public static ArrayList<Order> main() throws IOException {

        // Make a URL to the web page
        URL url = new URL("http://localhost:57714/api/orders");

        // Get the input stream through URL Connection
        URLConnection con = url.openConnection();
        InputStream is =con.getInputStream();

        // Once you have the Input Stream, it's just plain old Java IO stuff.

        // For this case, since you are interested in getting plain-text web page
        // I'll use a reader and output the text content to System.out.

        // For binary content, it's better to directly read the bytes from stream and write
        // to the target file.


        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line = null;
        ArrayList<Order> data = new ArrayList();

        while ((line = br.readLine()) != null) {
            String[] parts = line.split(",");
            String[] holding;
            String[] values;
            for (int j=0; j<(parts.length/8) ; j++){
                for (int i=(j*8); i<((j+1)*8); i++){
                    holding = parts[i].split(":");
                    values = holding[1].split(".");
                    parts[i] = values[0];
                }
                Order newOrder = new Order(Integer.parseInt(parts[0]),Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[3]), Integer.parseInt(parts[5]));
                System.out.println(parts[0]);
                System.out.println(parts[1]);
                System.out.println(parts[2]);
                System.out.println(parts[3]);
                System.out.println(parts[4]);
                System.out.println(parts[5]);
                System.out.println(line);
                data.add(newOrder);
            }
        }
        return data;
    }
}
