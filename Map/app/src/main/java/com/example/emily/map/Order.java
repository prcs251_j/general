package com.example.emily.map;

/**
 * Created by Emily on 29/04/2017.
 */

public class Order {
    private int orderNumber;
    private int pizzaNumber;
    private int orderStatus;
    private int orderTime;

    public Order(){
    }

    public Order(int num1, int num2, int piz, int orderState, int time){
        orderNumber = num1;
        pizzaNumber = piz;
        orderStatus = orderState;
        orderTime = time;
    }

}