package com.example.emily.navigationdrawer;

/**
 * Created by Emily on 30/04/2017.
 */

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PlaceholderFragment {
    public static   String TitleKey       = "sec_title";
    private         String activityName   = "Dummy fragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle                   savedInstanceState)
    {
        super.onCreateView(inflater,container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_placeholder, container, false):
        TextView tv = (TextView) rootView.findViewById(R.id.section_label);

        String title = getArguments().getString(TitleKey);
        tv.setText(title);

        return rootView;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        Log.i(activityName, "Paused");
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.i(activityName, "Resumed");
    }

    @Override
    public void onStop()
    {
        super.onStop();
        Log.i(activityName, "Stopped");
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.i(activityName, "Destroyed");
    }
}
