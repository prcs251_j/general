/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dwilde
 */
@Entity
@Table(name = "ORDERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orders.findAll", query = "SELECT o FROM Orders o"),
    @NamedQuery(name = "Orders.findByOrderid", query = "SELECT o FROM Orders o WHERE o.orderid = :orderid"),
    @NamedQuery(name = "Orders.findByOrderstatus", query = "SELECT o FROM Orders o WHERE o.orderstatus = :orderstatus"),
    @NamedQuery(name = "Orders.findByTotalCost", query = "SELECT o FROM Orders o WHERE o.totalCost = :totalCost"),
    @NamedQuery(name = "Orders.findByOrderTime", query = "SELECT o FROM Orders o WHERE o.orderTime = :orderTime"),
    @NamedQuery(name = "Orders.findByDeliveredtime", query = "SELECT o FROM Orders o WHERE o.deliveredtime = :deliveredtime"),
    @NamedQuery(name = "Orders.findByItemSize", query = "SELECT o FROM Orders o WHERE o.itemSize = :itemSize"),
    @NamedQuery(name = "Orders.findByItemid2", query = "SELECT o FROM Orders o WHERE o.itemid2 = :itemid2"),
    @NamedQuery(name = "Orders.findByItemSize2", query = "SELECT o FROM Orders o WHERE o.itemSize2 = :itemSize2"),
    @NamedQuery(name = "Orders.findByItemid3", query = "SELECT o FROM Orders o WHERE o.itemid3 = :itemid3"),
    @NamedQuery(name = "Orders.findByItemSize3", query = "SELECT o FROM Orders o WHERE o.itemSize3 = :itemSize3"),
    @NamedQuery(name = "Orders.findByItemid4", query = "SELECT o FROM Orders o WHERE o.itemid4 = :itemid4"),
    @NamedQuery(name = "Orders.findByItemSize4", query = "SELECT o FROM Orders o WHERE o.itemSize4 = :itemSize4"),
    @NamedQuery(name = "Orders.findByItemid5", query = "SELECT o FROM Orders o WHERE o.itemid5 = :itemid5"),
    @NamedQuery(name = "Orders.findByItemSize5", query = "SELECT o FROM Orders o WHERE o.itemSize5 = :itemSize5"),
    @NamedQuery(name = "Orders.findByItemid6", query = "SELECT o FROM Orders o WHERE o.itemid6 = :itemid6"),
    @NamedQuery(name = "Orders.findByItemSize6", query = "SELECT o FROM Orders o WHERE o.itemSize6 = :itemSize6"),
    @NamedQuery(name = "Orders.findByItemid7", query = "SELECT o FROM Orders o WHERE o.itemid7 = :itemid7"),
    @NamedQuery(name = "Orders.findByItemSize7", query = "SELECT o FROM Orders o WHERE o.itemSize7 = :itemSize7"),
    @NamedQuery(name = "Orders.findByItemid8", query = "SELECT o FROM Orders o WHERE o.itemid8 = :itemid8"),
    @NamedQuery(name = "Orders.findByItemSize8", query = "SELECT o FROM Orders o WHERE o.itemSize8 = :itemSize8"),
    @NamedQuery(name = "Orders.findByItemid9", query = "SELECT o FROM Orders o WHERE o.itemid9 = :itemid9"),
    @NamedQuery(name = "Orders.findByItemSize9", query = "SELECT o FROM Orders o WHERE o.itemSize9 = :itemSize9"),
    @NamedQuery(name = "Orders.findByItemid10", query = "SELECT o FROM Orders o WHERE o.itemid10 = :itemid10"),
    @NamedQuery(name = "Orders.findByItemSize10", query = "SELECT o FROM Orders o WHERE o.itemSize10 = :itemSize10")})
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORDERID")
    private BigDecimal orderid;
    @Column(name = "ORDERSTATUS")
    private BigInteger orderstatus;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_COST")
    private BigInteger totalCost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORDER_TIME")
    private BigInteger orderTime;
    @Column(name = "DELIVEREDTIME")
    private BigInteger deliveredtime;
    @Column(name = "ITEM_SIZE")
    private BigInteger itemSize;
    @Column(name = "ITEMID2")
    private BigInteger itemid2;
    @Column(name = "ITEM_SIZE2")
    private BigInteger itemSize2;
    @Column(name = "ITEMID3")
    private BigInteger itemid3;
    @Column(name = "ITEM_SIZE3")
    private BigInteger itemSize3;
    @Column(name = "ITEMID4")
    private BigInteger itemid4;
    @Column(name = "ITEM_SIZE4")
    private BigInteger itemSize4;
    @Column(name = "ITEMID5")
    private BigInteger itemid5;
    @Column(name = "ITEM_SIZE5")
    private BigInteger itemSize5;
    @Column(name = "ITEMID6")
    private BigInteger itemid6;
    @Column(name = "ITEM_SIZE6")
    private BigInteger itemSize6;
    @Column(name = "ITEMID7")
    private BigInteger itemid7;
    @Column(name = "ITEM_SIZE7")
    private BigInteger itemSize7;
    @Column(name = "ITEMID8")
    private BigInteger itemid8;
    @Column(name = "ITEM_SIZE8")
    private BigInteger itemSize8;
    @Column(name = "ITEMID9")
    private BigInteger itemid9;
    @Column(name = "ITEM_SIZE9")
    private BigInteger itemSize9;
    @Column(name = "ITEMID10")
    private BigInteger itemid10;
    @Column(name = "ITEM_SIZE10")
    private BigInteger itemSize10;
    @Column(name = "CUSTOMERID")
    private BigInteger customerid;
    @Column(name = "ITEMID")
    private BigInteger itemid;
    @Column(name = "STAFFID")
    private BigInteger staffid;


    public Orders() {
    }

    public Orders(BigDecimal orderid) {
        this.orderid = orderid;
    }

    public Orders(BigDecimal orderid, BigInteger totalCost, BigInteger orderTime) {
        this.orderid = orderid;
        this.totalCost = totalCost;
        this.orderTime = orderTime;
    }

    public BigDecimal getOrderid() {
        return orderid;
    }

    public void setOrderid(BigDecimal orderid) {
        this.orderid = orderid;
    }

    public BigInteger getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(BigInteger orderstatus) {
        this.orderstatus = orderstatus;
    }

    public BigInteger getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigInteger totalCost) {
        this.totalCost = totalCost;
    }

    public BigInteger getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(BigInteger orderTime) {
        this.orderTime = orderTime;
    }

    public BigInteger getDeliveredtime() {
        return deliveredtime;
    }

    public void setDeliveredtime(BigInteger deliveredtime) {
        this.deliveredtime = deliveredtime;
    }

    public BigInteger getItemSize() {
        return itemSize;
    }

    public void setItemSize(BigInteger itemSize) {
        this.itemSize = itemSize;
    }

    public BigInteger getItemid2() {
        return itemid2;
    }

    public void setItemid2(BigInteger itemid2) {
        this.itemid2 = itemid2;
    }

    public BigInteger getItemSize2() {
        return itemSize2;
    }

    public void setItemSize2(BigInteger itemSize2) {
        this.itemSize2 = itemSize2;
    }

    public BigInteger getItemid3() {
        return itemid3;
    }

    public void setItemid3(BigInteger itemid3) {
        this.itemid3 = itemid3;
    }

    public BigInteger getItemSize3() {
        return itemSize3;
    }

    public void setItemSize3(BigInteger itemSize3) {
        this.itemSize3 = itemSize3;
    }

    public BigInteger getItemid4() {
        return itemid4;
    }

    public void setItemid4(BigInteger itemid4) {
        this.itemid4 = itemid4;
    }

    public BigInteger getItemSize4() {
        return itemSize4;
    }

    public void setItemSize4(BigInteger itemSize4) {
        this.itemSize4 = itemSize4;
    }

    public BigInteger getItemid5() {
        return itemid5;
    }

    public void setItemid5(BigInteger itemid5) {
        this.itemid5 = itemid5;
    }

    public BigInteger getItemSize5() {
        return itemSize5;
    }

    public void setItemSize5(BigInteger itemSize5) {
        this.itemSize5 = itemSize5;
    }

    public BigInteger getItemid6() {
        return itemid6;
    }

    public void setItemid6(BigInteger itemid6) {
        this.itemid6 = itemid6;
    }

    public BigInteger getItemSize6() {
        return itemSize6;
    }

    public void setItemSize6(BigInteger itemSize6) {
        this.itemSize6 = itemSize6;
    }

    public BigInteger getItemid7() {
        return itemid7;
    }

    public void setItemid7(BigInteger itemid7) {
        this.itemid7 = itemid7;
    }

    public BigInteger getItemSize7() {
        return itemSize7;
    }

    public void setItemSize7(BigInteger itemSize7) {
        this.itemSize7 = itemSize7;
    }

    public BigInteger getItemid8() {
        return itemid8;
    }

    public void setItemid8(BigInteger itemid8) {
        this.itemid8 = itemid8;
    }

    public BigInteger getItemSize8() {
        return itemSize8;
    }

    public void setItemSize8(BigInteger itemSize8) {
        this.itemSize8 = itemSize8;
    }

    public BigInteger getItemid9() {
        return itemid9;
    }

    public void setItemid9(BigInteger itemid9) {
        this.itemid9 = itemid9;
    }

    public BigInteger getItemSize9() {
        return itemSize9;
    }

    public void setItemSize9(BigInteger itemSize9) {
        this.itemSize9 = itemSize9;
    }

    public BigInteger getItemid10() {
        return itemid10;
    }

    public void setItemid10(BigInteger itemid10) {
        this.itemid10 = itemid10;
    }

    public BigInteger getItemSize10() {
        return itemSize10;
    }

    public void setItemSize10(BigInteger itemSize10) {
        this.itemSize10 = itemSize10;
    }

    public BigInteger getCustomerid() {
        return customerid;
    }

    public void setCustomerid(BigInteger customerid) {
        this.customerid = customerid;
    }

    public BigInteger getItemid() {
        return itemid;
    }

    public void setItemid(BigInteger itemid) {
        this.itemid = itemid;
    }

    public BigInteger getStaffid() {
        return staffid;
    }

    public void setStaffid(BigInteger staffid) {
        this.staffid = staffid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderid != null ? orderid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orders)) {
            return false;
        }
        Orders other = (Orders) object;
        if ((this.orderid == null && other.orderid != null) || (this.orderid != null && !this.orderid.equals(other.orderid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Orders[ orderid=" + orderid + " ]";
    }
    
}
