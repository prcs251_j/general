A Pen created at CodePen.io. You can find this one at http://codepen.io/emgerold/pen/kjivC.

 Source: http://gmaps-samples-v3.googlecode.com/svn/trunk/infowindow_custom/infowindow-custom.html

I've changed the code, so that multiple markers are possible and infobox styling is now done with css.