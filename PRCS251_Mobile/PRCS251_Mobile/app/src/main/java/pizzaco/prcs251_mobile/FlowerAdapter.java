package pizzaco.prcs251_mobile;

/**
 * Created by erharrison on 08/05/2017.
 */

import android.content.Context;

import android.view.LayoutInflater;

import android.view.View;

import android.view.ViewGroup;

import android.widget.ArrayAdapter;

import android.widget.TextView;

import java.util.ArrayList;

/**

 * Created by erharrison on 8/5/2017.

 */
/*We create our own adapter class that extends ArrayAdapter, with data type of our Flower class.

        The constructor gets parameters that are passed with instance of FlowerAdapter .*/

public class FlowerAdapter extends ArrayAdapter<Flowers> {

    private  ArrayList<Flowers> items;

    private Context mContext;

    public FlowerAdapter(Context context, int textViewResourceID, ArrayList<Flowers> items){

        super(context,textViewResourceID,items);

        mContext = context;

        this.items = items;

    }

    @Override

    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        Flowers flowers = items.get(position);

        if(v==null){

            LayoutInflater inflater =(LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v=inflater.inflate(R.layout.flower_list_item,null);

        }

        TextView title = (TextView)v.findViewById(R.id.textView3);

        if (title != null) {

            title.setText(flowers.getName());

        }

        return v;

    }

}
