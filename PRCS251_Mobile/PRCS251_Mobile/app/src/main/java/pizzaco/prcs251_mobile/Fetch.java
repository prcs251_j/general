package pizzaco.prcs251_mobile;

        import android.app.Activity;

        import android.os.AsyncTask;

        import android.os.Bundle;

        import android.text.method.ScrollingMovementMethod;

        import android.util.Log;

        import android.widget.ListView;

        import android.widget.TextView;

        import org.json.JSONArray;

        import org.json.JSONException;

        import org.json.JSONObject;

        import java.net.HttpURLConnection;

        import java.util.ArrayList;

/**

 * Created by erharrison on 8/5/2017.

 */

public class Fetch extends Activity {

    ArrayList<Flowers> flowersList = new ArrayList<Flowers>();

    String url ="http://services.hanselandpetal.com/feeds/flowers.json";

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.fetch);

        new BackTask().execute(url);

    }

    public class BackTask extends AsyncTask<String,String,String>{

        @Override

        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override

        protected String doInBackground(String... strings) {

            String content =HttpURLConnect.getData(url);

            return content;

        }

       /* In Fetch.java class with in our inner class, onPostExecute() method the data is parsed, and then added to the flower list.
        Since we get the data in json array , we will need to first parse the data, in order to use it .*/
        @Override

        protected void onPostExecute(String s) {

            try {

                JSONArray ar = new JSONArray(s);

                for (int i=0; i<ar.length(); i++){

                    JSONObject jsonobject = ar.getJSONObject(i);

                    Flowers  flowers = new Flowers();

                    flowers.setName(jsonobject.getString("name"));

                    flowersList.add(flowers);

                }

            }

            catch (JSONException e){

                e.printStackTrace();

            }

            FlowerAdapter adapter = new FlowerAdapter(Fetch.this, R.layout.flower_list_item, flowersList);

            ListView lv = (ListView) findViewById(R.id.listView);

            lv.setAdapter(adapter);

//Log.d("recived",s);

        }

    }

}
