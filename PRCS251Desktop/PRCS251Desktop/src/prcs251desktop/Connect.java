/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prcs251desktop;

/**
 *
 * @author decla_000
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import static java.util.Arrays.fill;
import java.util.List;

public class Connect {
                
    private String sendGet(URL url) throws IOException{
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("charset", "utf-8");
        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code: " + responseCode);        
        InputStream is =con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        return br.readLine();
    }
    
    
    /**
     * Method which gets the data from the order table of the database
     * @return ArrayList<Order> being an array list of Order instances
     */
    public ArrayList<Order> getOrder() throws IOException {

        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.orders");

        String line = sendGet(url);
        
        ArrayList<Order> data = new ArrayList();
        
            System.out.println(line);
            String[] holdLine = line.split("<orderss><orders>");
            String[] removeEnd = holdLine[1].split("</orderss>");
            String[] parts = removeEnd[0].split("</orders><orders>");
            String[] holder = new String[1];
            List<String> orderValues = new ArrayList<String>();
            for (int i = 0; i<parts.length; i++)
            {
                holdLine[0] = parts[i];
                String[] splitLine = holdLine[0].split(">");
                for (int j = 1; j<splitLine.length;j++){
                    holder = splitLine[j].split("<");
                    orderValues.add(holder[0]);
                    j++;
                }
                if (Integer.parseInt(orderValues.get(orderValues.size()-3)) == 0){
                    int items = ((orderValues.size() -6)/2);
                    int[] itemIds = new int[items];
                    int[] itemSizes = new int[items];
                    for (int k=1; k<items+1;k++){
                        itemIds[k-1] = Integer.parseInt(orderValues.get(k));
                    }
                    int l=0;
                    for (int k = 1+items; k<(1+items+items);k++){
                        itemSizes[l] = Integer.parseInt(orderValues.get(k));
                        l++;
                    }
                    int size = orderValues.size();
                    Order newOrder = new Order(Integer.parseInt(orderValues.get(size-4)),Integer.parseInt(orderValues.get(0)),Integer.parseInt(orderValues.get(size-3)),Integer.parseInt(orderValues.get(size-5)),Integer.parseInt(orderValues.get(size-1)),Integer.parseInt("0"),Integer.parseInt(orderValues.get(size-2)),itemIds,itemSizes);
                    data.add(newOrder);
                }
                orderValues.clear();
            }
        return data;
    }
    
    /**
     * Method which gets the data from the Menu table of the database
     * @return ArrayList<Pizza> being an array list of Pizza instances
     */
    public ArrayList<Pizza> getPizza()throws IOException{
        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.menu");
        
        String line = sendGet(url);
        
        ArrayList<Pizza> data = new ArrayList();
        
            System.out.println(line);
            String[] parts = line.split("<");
            String[] holder = new String[1];
            List<String> pizzaParts = new ArrayList<String>();
            for (int i = 3; i<(parts.length-2); i++)
            {
                for (int k=(i+1);k<(i+6);k++){ 
                    holder = parts[k].split(">");
                    pizzaParts.add(holder[1]);
                    k++;
                }
                i +=7;
            }
            for (int j=0; j<pizzaParts.size() ; j++){
                Pizza newPizza = new Pizza(Integer.parseInt(pizzaParts.get(j)),pizzaParts.get(j+1),pizzaParts.get(j+2));
                data.add(newPizza);
                j+=2;
            }
        return data;
    }
    
    public ArrayList<Customer> getCustomer()throws IOException{
        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.customer");
        
        String line = sendGet(url);
        
        ArrayList<Customer> data = new ArrayList();
        
            System.out.println(line);
            String[] parts = line.split("<");
            String[] holder = new String[1];
            List<String> customerParts = new ArrayList<String>();
            for (int i = 3; i<(parts.length-2); i++)
            {
                for (int k=(i+1);k<(i+8);k++){ 
                    holder = parts[k].split(">");
                    customerParts.add(holder[1]);
                    k++;
                }
                i +=9;
            }
            for (int j=0; j<customerParts.size() ; j++){
                Customer newCustomer = new Customer(customerParts.get(j),Integer.parseInt(customerParts.get(j+1)),Integer.parseInt(customerParts.get(j+2)),customerParts.get(j+3));
                data.add(newCustomer);
                j+=3;
            }
        return data;
    }
    
    
    /**
     * Method which gets the data from the Staff table of the database
     * @return ArrayList<Staff> being an array list of Staff instances
     */
    public ArrayList<Staff> getStaff()throws IOException{
        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.staff");

        String line = sendGet(url);
        
        ArrayList<Staff> data = new ArrayList();
        
            System.out.println(line);
            String[] parts = line.split("<");
            String[] holder = new String[1];
            List<String> staffParts = new ArrayList<String>();
            for (int i = 3; i<(parts.length-4); i++)
            {
                for (int k=(i+1);k<(i+14);k++){ 
                    holder = parts[k].split(">");
                    staffParts.add(holder[1]);
                    k++;
                }
                i +=15;
            }
            for (int j=0; j<staffParts.size() ; j++){
                boolean manager = false;
                if (Integer.parseInt(staffParts.get(j+3)) == 1){
                    manager = true;
                }
                boolean available = false;
                if (Integer.parseInt(staffParts.get(j+2)) == 1){
                    available = true;
                }
                Staff newStaff = new Staff(Integer.parseInt(staffParts.get(j+4)),staffParts.get(j),manager,available,staffParts.get(j+1),staffParts.get(j+6),staffParts.get(j+5));
                data.add(newStaff);
                j+=6;
            }
        return data;
    }
    
    private void sendPost(URL url, String toPost)throws IOException{
        HttpURLConnection test = (HttpURLConnection) url.openConnection();
        test.setDoOutput(true);       
        test.setRequestMethod("POST");
        test.setRequestProperty("User-Agent", "USER_AGENT");
        test.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        test.setRequestProperty("Content-Type", "application/xml");
        test.setRequestProperty("charset", "utf-8");
        OutputStreamWriter    out = new OutputStreamWriter   (test.getOutputStream());
        out.write(toPost);
        out.flush();
        int responseCode2 = test.getResponseCode();


        out.close();
        
        System.out.println("\nSending 'POST' request to URL : " + url);
	System.out.println("Response Code : " + responseCode2);
    }
    
    private void sendPut(URL url, String toPost) throws IOException{
        HttpURLConnection test = (HttpURLConnection) url.openConnection();
        test.setDoOutput(true);       
        test.setRequestMethod("PUT");
        test.setRequestProperty("User-Agent", "USER_AGENT");
        test.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        test.setRequestProperty("Content-Type", "application/xml");
        test.setRequestProperty("charset", "utf-8");
        OutputStreamWriter    out = new OutputStreamWriter   (test.getOutputStream());
        out.write(toPost);
        out.flush();
        int responseCode2 = test.getResponseCode();


        out.close();
        
        System.out.println("\nSending 'PUT' request to URL : " + url);
	System.out.println("Response Code : " + responseCode2);
    }
    
    /**
     * Method which Posts an order to the database
     * @param orderToSend Order class, being the order to Post to the database
     * @param pizzaData ArrayList<Pizza>, being a list of Pizza taken from the database
     */
    public void returnOrder(Order orderToSend)throws IOException{
        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.orders");

        String testString = wrapOrder(orderToSend);

        sendPost(url, testString);
    }
    
    /**
     * Method which Posts a Pizza to the database
     * @param toSend Pizza class, to be Post to the database
     */
    public void returnMenu(Pizza toSend) throws IOException {
        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.menu");
            
        String stringToSend = "<menu>" + wrapPizza(toSend) + "</menu>";

        sendPost(url, stringToSend);
    }
    
    /**
     * Method which Posts a Pizza to the database
     * @param toSend Pizza class, to be Post to the database
     */
    public void putMenu(Pizza toSend) throws IOException {
        int id = toSend.getPizzaId();

            
        String stringToSend = "<menu>" + wrapPizza(toSend) + "</menu>";
        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.menu/" + id);
        sendPut(url, stringToSend);
    }
    
    /**
     * Method which Puts a Staff member to the database
     * @param toSend Staff class, to be Put to the database
     */
    public void returnStaff(Staff toSend) throws IOException {
        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.staff");

        String stringToSend = "<staff>" + wrapStaff(toSend) + "</staff>";
        
        sendPost(url, stringToSend);
    }
    
    public void putStaff(Staff toSend) throws IOException {
        int id = toSend.getStaffId();
        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.staff/" + id);
            
        String stringToSend = "<staff>" + wrapStaff(toSend) + "</staff>";

        sendPut(url, stringToSend);
    }
    
    public void putOrder(Order toSend) throws IOException {
        int id = toSend.getOrderId();
        URL url = new URL("http://eeyore.fost.plymouth.ac.uk:8082/PRCS251J_MiddleWare/webresources/entities.orders/" + id);
            
        String stringToSend = "<orders>" + wrapOrder(toSend) + "</orders>";

        sendPut(url, stringToSend);
    }
    
    private String wrapPizza(Pizza pizzaToWrap){
        String returnString;
        returnString = "<foodid>"+pizzaToWrap.getPizzaId()+"</foodid><pizzaName>"+pizzaToWrap.getName()+"</pizzaName><price>"+pizzaToWrap.getPrice()+"</price>";
        return returnString;
    }
    
    private String wrapStaff(Staff staffToWrap){
        String returnString;
        returnString = "<drivername>"+staffToWrap.getName()+"</drivername><driverstatus>"+staffToWrap.getStatus()+"</driverstatus><isavailable>";
        if (staffToWrap.isAvailable() == true){
            returnString += 1;
        }
        else{
            returnString += 0;
        }
        returnString +="</isavailable><ismanager>";
        if (staffToWrap.isManager() == true){
            returnString += 1;
        }
        else{
            returnString += 0;
        }
        returnString +="</ismanager><staffid>"+staffToWrap.getStaffId()+"</staffid><staffpassword>"+staffToWrap.getPassword()+"</staffpassword><username>"+staffToWrap.getUsername()+"</username>";
        return returnString;
    }
    
    private String wrapCustomer(Customer customerToWrap){
        String returnString;
        returnString = "<address>"+customerToWrap.getAddress()+"</address><customerid>"+customerToWrap.getCustomerId()+"</customerid><orderHistory>"+customerToWrap.getOrderHistory()+"</orderHistory>"+
"<postCode>"+customerToWrap.getPostCode()+"</postCode>";
        return returnString;
    }
    
    private String wrapOrder(Order orderToWrap){
        String returnString;
        returnString = "<customerid>"+orderToWrap.getCustomerId()+"</customerid><deliveredtime>"+orderToWrap.getDeliveredTime()+"</deliveredtime>";
        
        int[] itemSizes = orderToWrap.getItemSizes();
        returnString += "<itemSize>" + itemSizes[0] +"</itemSize>";
        if (itemSizes.length > 1){
            for (int i = 1; i < itemSizes.length; i++){
		returnString += "<itemSize" + i + ">" + itemSizes[i] + "</itemSize" + i + ">";
            }
        }
        
        int[] itemIds = orderToWrap.getItemIds();
        returnString += "<itemid>" + itemIds[0] + "</itemid>";
        if (itemIds.length > 1){
            for (int i = 1; i < itemIds.length; i++){
		returnString += "<itemid" + i + ">" + itemIds[i] + "</itemSize" + i + ">";
            }
        }
        
        returnString +="<orderTime>"+orderToWrap.getOrderTime()+"</orderTime><orderid>"+orderToWrap.getOrderId()+"</orderid><orderstatus>"+orderToWrap.getOrderStatus();
        returnString +="</orderstatus><staffid>"+orderToWrap.getStaffId()+"</staffid><totalCost>"+orderToWrap.getTotalCost()+"</totalCost>";
        return returnString;
    }
    
}