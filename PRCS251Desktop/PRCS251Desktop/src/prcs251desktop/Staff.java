/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prcs251desktop;

/**
 *
 * @author decla_000
 */
public class Staff {
    private int staffId;
    private String name;
    private boolean manager;
    private boolean available;
        
    private String status;
    private String username;
    private String password;
        
   /**
    * Basic constructor used for serialization
    */
    public Staff(){}
        
    public Staff(int id, String strin, boolean type, boolean signedin){
        staffId = id;
        name = strin;
        manager = type;
        available = signedin;
    }
        
    public Staff(int id, String strin, boolean type, boolean signedin, String state, String user, String pass){
        staffId = id;
        name = strin;
        manager = type;
        available = signedin;
    
        status = state;
        username = user;
        password = pass;
    }

    /**
     * Accessor to get the status of the staff member
     * @return String being the status of the staff member
     */
    public String getStatus() {
        return status;
    }

    /**
     * Accessor to get the username of the staff member
     * @return String being the username of the staff member
     */
    public String getUsername() {
        return username;
    }


    /**
     * Accessor to get the password of the staff member
     * @return String being the password of the staff member
     */
    public String getPassword() {
        return password;
    }

    /**
     * Accessor to get the Id of the staff member
     * @return Integer being the Id of the staff member
     */
    public int getStaffId() {
        return staffId;
    }


    /**
     * Accessor to get the name of the staff member
     * @return String being the name of the staff member
     */
    public String getName() {
        return name;
    }

    /**
     * Accessor to set the name of the staff member
     * @param name String, being the name of the staff member
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Accessor to get if the staff member is a manager
     * @return boolean being true if the staff member is a manager
     */
    public boolean isManager() {
        return manager;
    }

    /**
     * Accessor to set if the staff member is a manager
     * @param manager boolean, being being true if the staff member is a manager
     */
    public void setManager(boolean manager) {
        this.manager = manager;
    }

    /**
     * Accessor to get if the staff member is available
     * @return boolean being true if the staff member is available
     */
    public boolean isAvailable() {
        return available;
    }
}
