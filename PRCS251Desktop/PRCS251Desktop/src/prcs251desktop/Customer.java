/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prcs251desktop;

/**
 *
 * @author decla_000
 */
public class Customer {
    private String address;
    private int customerId;
    private int orderHistory;
    private String postCode;
    
    /**
     * Basic constructor used for serialization
     */
    public Customer(){}
    
    public Customer(String ad, int id, int history, String post){
        address = ad;
        customerId = id;
        orderHistory = history;
        postCode = post;
    }

    /**
     * Accessor to get the address of the customer
     * @return String being the address of the customer
     */
    public String getAddress() {
        return address;
    }

    /**
     * Accessor to set the address of the customer
     * @param address String, being the address of the customer
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Accessor to get the ID of the customer
     * @return integer being the ID of the customer
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Accessor to set the ID of the customer
     * @param customerId integer, being the ID of the customer
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     * Accessor to get the order history number of the customer
     * @return integer being the order history number
     */
    public int getOrderHistory() {
        return orderHistory;
    }

    /**
     * Accessor to set the order history number of the customer 
     * @param orderHistory integer, being the order history number of the customer
     */
    public void setOrderHistory(int orderHistory) {
        this.orderHistory = orderHistory;
    }

    /**
     * Accessor to get the postcode of the customer
     * @return String being the postcode of the customer
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Accessor to set the postcode of the customer 
     * @param postCode String, being the postcode of the customer
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
}
