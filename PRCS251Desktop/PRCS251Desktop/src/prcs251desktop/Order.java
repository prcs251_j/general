/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prcs251desktop;

/**
 *
 * @author dwilde
 */
public class Order { 
    private int orderId;
    private int customerId;
    private int orderStatus;
    private int totalCost;
    private int orderTime;
    private int deliveredTime;
    private int staffId;
    private int[] itemIds = new int[10];
    private int[] itemSizes = new int[10];

    /**
     * Accessor to get the item ID array for the order
     * @return Integer[] being an integer array of up to 10 Item IDs
     */
    public int[] getItemIds() {
        return itemIds;
    }

    /**
     * Accessor to set the item ID array for the order
     * @param itemIds Integer[], being an Integer array of up to 10 Item IDs
     */
    public void setItemIds(int[] itemIds) {
        this.itemIds = itemIds;
    }
    
    /**
     * Accessor to get the item sizes array for the order
     * @return Integer[] being an integer array of up to 10 Item sizes
     */
    public int[] getItemSizes() {
        return itemSizes;
    }

    /**
     * Accessor to set the item sizes array for the order
     * @param itemSizes Integer[], being an Integer array of up to 10 Item sizes
     */
    public void setItemSizes(int[] itemSizes) {
        this.itemSizes = itemSizes;
    }
    
    /**
     * Accessor to get the ID of the order's customer
     * @return Integer being the ID of the customer
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Accessor to set the ID of the order's customer
     * @param customerId Integer, being the ID of the customer
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     * Accessor to get the order status
     * @return Integer being the status of the order
     */
    public int getOrderStatus() {
        return orderStatus;
    }

    /**
     * Accessor to set the order status
     * @param orderStatus Integer, being the status of the order
     */
    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * Accessor to get the total cost of the order
     * @return Integer being the total cost of the order
     */
    public int getTotalCost() {
        return totalCost;
    }

    /**
     * Accessor to set the total cost of the order
     * @param totalCost Integer, being the total cost of the order
     */
    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    /**
     * Accessor to get the delivery time of the order
     * @return Integer being the delivery time of the order
     */
    public int getDeliveredTime() {
        return deliveredTime;
    }

    /**
     * Accessor to get the ID of the staff member handling the order
     * @return Integer being the ID of the staff member
     */
    public int getStaffId() {
        return staffId;
    }

    /**
     * Basic constructor used for serialization
     */
    public Order(){
    }
    
    public Order(int num1, int num2, int orderState, int time){
        orderId = num1;
        customerId = num2;
        orderStatus = orderState;
        orderTime = time;
    }
    
    public Order(int num1, int num2, int orderState, int time, int cost, int delivered, int staff){
        orderId = num1;
        customerId = num2;
        orderStatus = orderState;
        orderTime = time;
        totalCost = cost;
        deliveredTime = delivered;
        staffId = staff;
    }
    
    public Order(int num1, int num2, int orderState, int time, int cost, int delivered, int staff, int[] ids, int[] sizes){
        orderId = num1;
        customerId = num2;
        orderStatus = orderState;
        orderTime = time;
        totalCost = cost;
        deliveredTime = delivered;
        staffId = staff;
        itemIds = ids;
        itemSizes = sizes;
    }
    
    /**
     * Accessor to get the ID of the order
     * @return Integer being the ID of the order
     */
    public int getOrderId(){
        return orderId;
    }
    
    /**
     * Accessor to get the time the order was made
     * @return Integer being the time the order was made
     */
    public int getOrderTime(){
        return orderTime;
    }
    
    /**
     * Accessor to get the status of the order
     * @return Integer being the status of the order
     */
    public int getStatus(){
        return orderStatus;
    }
    
    /**
     * Accessor to set the status of the order
     * @param orderState Integer, being the status of the order
     */
    public void setStatus(int orderState){
        orderStatus = orderState;
    }
}
