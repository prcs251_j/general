/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prcs251desktop;

/**
 *
 * @author dwilde
 */
public class Pizza {
    private int pizzaId;
    private String name;
    private String price;
   
    
    /**
     * Basic constructor used for serialization
     */
    public Pizza(){}
    
    public Pizza(int id, String nam, String pri){
        pizzaId = id;
        name = nam;
        price = pri;
    }

    /**
     * Accessor to get the ID of the item
     * @return Integer being the ID of the item
     */
    public int getPizzaId() {
        return pizzaId;
    }

    /**
     * Accessor to set the Id of the item
     * @param pizzaId Integer, being the Id of the item
     */
    public void setPizzaId(int pizzaId) {
        this.pizzaId = pizzaId;
    }

    /**
     * Accessor to get the name of the item
     * @return String being the name of the item
     */
    public String getName() {
        return name;
    }

    /**
     * Accessor to set the name of the item
     * @param name String, being the name of the item
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Accessor to get the price of the item
     * @return String being the price of the item
     */
    public String getPrice() {
        return price;
    }

    /**
     * Accessor to set the price of the item
     * @param price String, being the price of the item
     */
    public void setPrice(String price) {
        this.price = price;
    }
}
